package app;

import java.sql.Connection;
import java.sql.SQLException;

public class App {

	public static void main(String[] args) {

		// Ejemplo1: conexión a base de datos
		try {
			Connection con1 = ConexionBD.getInstance().getConexion();
			Connection con2 = ConexionBD.getInstance().getConexion();
			System.out.println("Son la misma conexión: " + (con1 == con2));
		} catch (SQLException e) {
			System.out.println("Error conexión");
		}

		
		// Ejemplo2: recurso único
		Impresora impresora1 = Impresora.getInstance();
		Impresora impresora2 = Impresora.getInstance();
		System.out.println("Son el mismo recurso impresora: " + (impresora1 == impresora2));
		impresora1.imprimir("Wilfredo colgó un wav wifi en la world wide web");
		impresora2.imprimir("¿Qué wav colgó con su wifi wilfredo en la world wide web?");

	}

}
