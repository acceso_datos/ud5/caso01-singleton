package app;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

// Patrón singleton clásico: instanciación perezosa.
// La instancia de singleton no se crea hasta que se llama al método getInstance() 
// por primera vez. Esta técnica garantiza que las instancias singleton se creen solo cuando sea necesario.
// Como se puede observar, si seguimos el patrón Singleton estricto, la clase ConexiónBD que hemos usado hasta ahora, aunque cumple
// nuestro propósito, no cumpliría completamente los puntos clave del patrón:
// - No garantiza la unicidad del objeto como un Singleton estricto. Según el patrón Singleton, la clase debe controlar completamente la creación de su única instancia, no solo de un recurso compartido. 
// - Faltaría constructor privado. El patrón Singleton requiere un constructor privado para evitar que se creen instancias adicionales de la clase.
// - Faltaría acceso explícito a la instancia de la clase. Normalmente, el Singleton tiene un método estático como getInstance() que devuelve la instancia única de la clase. 

public class ConexionBD {
    private static final String JDBC_URL = "jdbc:mariadb://192.168.56.103:3306/empresa";
    private static ConexionBD instancia = null;
    private Connection con;

    // Constructor privado para evitar instanciación externa
    private ConexionBD() throws SQLException {
        Properties pc = new Properties();
        pc.put("user", "batoi");
        pc.put("password", "1234");
        con = DriverManager.getConnection(JDBC_URL, pc);
    }

    // Método estático para obtener la instancia única: compatible con entornos multihilo
    public static synchronized ConexionBD getInstance() throws SQLException {
        if (instancia == null) {
            instancia = new ConexionBD();
        }
        return instancia;
    }

    // Método para obtener la conexión
    public Connection getConexion() {
        return con;
    }

    // Método para cerrar la conexión
    public void cerrar() throws SQLException {
        if (con != null) {
            con.close();
        }
        instancia = null; // Liberar la instancia si es necesario
    }
}

