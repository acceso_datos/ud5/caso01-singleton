package app;

// Patrón singleton
// La implementación singleton más sencilla conocida como instanciación temprana (eager) 
// consiste en un constructor privado, un campo para contener una instancia y 
// un método público de acceso estático con un nombre como getInstance(). 
public class Impresora {
    private String modelo = "HP LaserJet M402";

    // Instancia única creada de forma estática
    private static Impresora impresora = new Impresora();

    // Constructor privado
    private Impresora() {
    }

    // Método estático para obtener la instancia única
    public static Impresora getInstance() {
        return impresora;
    }

    // Método para imprimir
    public void imprimir(String texto) {
        System.out.println("Impresora " + modelo + "... imprimiendo: " + texto);
    }
}



// Versión con instanciación perezosa:
/* 
public class Impresora {
    private String modelo = "HP LaserJet M402";
    private static Impresora impresora = null;

    private Impresora() {
    }

    public static synchronized Impresora getInstance() {
        if (impresora == null) {
            impresora = new Impresora();
        }
        return impresora;
    }

    public void imprimir(String texto) {
        System.out.println("Impresora " + modelo + "... imprimiendo: " + texto);
    }
} */
